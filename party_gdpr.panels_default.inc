<?php

/**
 * @file
 * party_gdpr.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function party_gdpr_default_panels_mini() {
  $export = array();

  // Scan directory for any .panel files
  $files = file_scan_directory(dirname(__FILE__) . '/panels_mini', '/\.panel$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ((include $file->uri) == 1) {
      $export[$mini->name] = $mini;
    }
  }

  return $export;
}

/**
 * Implements hook_default_panels_mini_alter().
 *
 * Override the default party dashboard summary and party view mini panels.
 */
function party_gdpr_default_panels_mini_alter(&$export) {
  // Scan this directory for any .panel_alter files
  $files = file_scan_directory(dirname(__FILE__) . '/panels_mini', '/\.panel_alter$/', array('key' => 'name'));
  foreach ($files as $file) {
    if (isset($export[$file->name])) {
      $mini = &$export[$file->name];
      include $file->uri;
    }
  }
}
