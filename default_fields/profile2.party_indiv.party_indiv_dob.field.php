<?php
$field = new GDPRFieldData();
$field->disabled = FALSE; /* Edit this to true to make a default field disabled initially */
$field->name = 'profile2|party_indiv|party_indiv_dob';
$field->plugin_type = 'gdpr_entity_field';
$field->entity_type = 'profile2';
$field->entity_bundle = 'party_indiv';
$field->field_name = 'party_indiv_dob';
$field->settings = array(
  'gdpr_fields_enabled' => '1',
  'gdpr_fields_rta' => 'inc',
  'gdpr_fields_rtf' => 'remove',
  'gdpr_fields_sanitizer' => '',
  'notes' => '',
  'label' => 'Date of Birth',
  'description' => NULL,
);
