<?php
$field = new GDPRFieldData();
$field->disabled = FALSE; /* Edit this to true to make a default field disabled initially */
$field->name = 'profile2|party_indiv|party_indiv_name';
$field->plugin_type = 'gdpr_entity_field';
$field->entity_type = 'profile2';
$field->entity_bundle = 'party_indiv';
$field->field_name = 'party_indiv_name';
$field->settings = array(
  'gdpr_fields_enabled' => '1',
  'gdpr_fields_rta' => 'inc',
  'gdpr_fields_rtf' => 'anonymise',
  'gdpr_fields_sanitizer' => 'gdpr_sanitizer_name',
  'notes' => '',
  'label' => 'Name',
  'description' => NULL,
);
